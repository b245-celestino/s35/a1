const { request } = require("express");
const express = require("express");

const mongoose = require("mongoose");

const port = 3001;
const app = express();

mongoose.connect(
  "mongodb+srv://admin:admin@batch245-celestino.ldu2t4p.mongodb.net/s35-discussion?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
// error handling in connecting
db.on("error", console.error.bind(console, "Connection error"));
// this will be triggered if the connection is successful
db.once("open", () => console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Task name is required!"],
  },
  status: {
    type: String,
    default: "pending",
  },
});

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "Username is required!"],
  },
  password: {
    type: String,
    required: [true, "Password is required!"],
  },
});

const Task = mongoose.model("Task", taskSchema);

const User = mongoose.model("User", userSchema);

// middlewares
app.use(express.json()); //allows the app to read the json data
app.use(express.urlencoded({ extended: true })); //it will allow our app to read data from forms.

//routing
// create/add new task

app.post("/tasks", (request, response) => {
  let input = request.body;

  console.log(input.status);
  if (input.status === undefined) {
    Task.findOne({ name: input.name }, (error, result) => {
      console.log(result);
      if (result !== null) {
        return response.send("The task is already existing!");
      } else {
        let newTask = new Task({
          name: input.name,
        });
        //  save() method will save the object in the collection that the object instatiated.
        newTask.save((saveError, savedTask) => {
          if (saveError) {
            return console.log(saveError);
          } else {
            return response.send("New task created!");
          }
        });
      }
    });
  } else {
    Task.findOne({ name: input.name }, (error, result) => {
      console.log(result);
      if (result !== null) {
        return response.send("The task is already existing!");
      } else {
        let newTask = new Task({
          name: input.name,
          status: input.status,
        });
        //  save() method will save the object in the collection that the object instatiated.
        newTask.save((saveError, savedTask) => {
          if (saveError) {
            return console.log(saveError);
          } else {
            return response.send("New task created!");
          }
        });
      }
    });
  }
});

// retrieving all the task

app.get("/tasks", (request, response) => {
  Task.find({}, (error, result) => {
    if (error) {
      console.log(error);
    } else {
      return response.send(result);
    }
  });
});

// POST route to access the /signup

app.post("/signup", (request, response) => {
  let input = request.body;

  console.log(input.password);
  if (input.password === undefined) {
    User.findOne({ username: input.username }, (error, result) => {
      console.log(result);
      if (result !== null) {
        return response.send("The username is already existing!");
      } else {
        let newUser = new User({
          username: input.username,
          password: input.password,
        });

        newUser.save((saveError, savedUser) => {
          if (saveError) {
            return console.log(saveError);
          } else {
            return response.send("New username is created!");
          }
        });
      }
    });
  } else {
    User.findOne({ username: input.username }, (error, result) => {
      console.log(result);
      if (result !== null) {
        return response.send("The username is already existing!");
      } else {
        let newUser = new User({
          username: input.username,
          password: input.password,
        });

        newUser.save((saveError, savedTask) => {
          if (saveError) {
            return console.log(saveError);
          } else {
            return response.send("New username is created!");
          }
        });
      }
    });
  }
});

// retrieving all the users

app.get("/signup", (request, response) => {
  User.find({}, (error, result) => {
    if (error) {
      console.log(error);
    } else {
      return response.send(result);
    }
  });
});

app.listen(port, () => console.log(`Server is running at port ${port}!`));
